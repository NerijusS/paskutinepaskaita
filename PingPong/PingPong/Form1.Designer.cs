﻿namespace PingPong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.playground = new System.Windows.Forms.Panel();
            this.rakete = new System.Windows.Forms.PictureBox();
            this.ball = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.taskai = new System.Windows.Forms.Label();
            this.task = new System.Windows.Forms.Label();
            this.game_over = new System.Windows.Forms.Label();
            this.playground.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rakete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).BeginInit();
            this.SuspendLayout();
            // 
            // playground
            // 
            this.playground.Controls.Add(this.game_over);
            this.playground.Controls.Add(this.task);
            this.playground.Controls.Add(this.taskai);
            this.playground.Controls.Add(this.ball);
            this.playground.Controls.Add(this.rakete);
            this.playground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playground.Location = new System.Drawing.Point(0, 0);
            this.playground.Name = "playground";
            this.playground.Size = new System.Drawing.Size(781, 382);
            this.playground.TabIndex = 0;
            // 
            // rakete
            // 
            this.rakete.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.rakete.Location = new System.Drawing.Point(308, 359);
            this.rakete.Name = "rakete";
            this.rakete.Size = new System.Drawing.Size(200, 20);
            this.rakete.TabIndex = 0;
            this.rakete.TabStop = false;
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.Red;
            this.ball.Location = new System.Drawing.Point(336, 205);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(30, 30);
            this.ball.TabIndex = 1;
            this.ball.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // taskai
            // 
            this.taskai.AutoSize = true;
            this.taskai.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.taskai.Location = new System.Drawing.Point(4, 4);
            this.taskai.Name = "taskai";
            this.taskai.Size = new System.Drawing.Size(139, 42);
            this.taskai.TabIndex = 2;
            this.taskai.Text = "Taskai:";
            // 
            // task
            // 
            this.task.AutoSize = true;
            this.task.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.task.Location = new System.Drawing.Point(139, 9);
            this.task.Name = "task";
            this.task.Size = new System.Drawing.Size(36, 39);
            this.task.TabIndex = 3;
            this.task.Text = "0";
            // 
            // game_over
            // 
            this.game_over.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.game_over.AutoSize = true;
            this.game_over.Location = new System.Drawing.Point(396, 143);
            this.game_over.Name = "game_over";
            this.game_over.Size = new System.Drawing.Size(81, 65);
            this.game_over.TabIndex = 4;
            this.game_over.Text = "Game Over\r\n\r\nSpace - Restart\r\n\r\nEsc - Exit\r\n";
            this.game_over.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 382);
            this.Controls.Add(this.playground);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.playground.ResumeLayout(false);
            this.playground.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rakete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel playground;
        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.PictureBox rakete;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label task;
        private System.Windows.Forms.Label taskai;
        private System.Windows.Forms.Label game_over;
    }
}

