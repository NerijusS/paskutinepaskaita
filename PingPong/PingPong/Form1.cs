﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        public int speed_left = 4;
        public int speed_top = 4;
        public int point = 0;

        public Form1()
        {
            InitializeComponent();

            timer1.Enabled = true;
            Cursor.Hide();

            this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true;
            this.Bounds = Screen.PrimaryScreen.Bounds;

            rakete.Top = playground.Bottom - (playground.Bottom / 10);

            game_over.Left = (playground.Width / 2) - (game_over.Width / 2);
            game_over.Top = (playground.Height / 2) - (game_over.Height / 2);
            game_over.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            rakete.Left = Cursor.Position.X - (rakete.Width / 2);

            ball.Left += speed_left;
            ball.Top += speed_top;


            if (ball.Bottom >= rakete.Top && ball.Bottom <= rakete.Bottom && ball.Left >= rakete.Left && ball.Right <= rakete.Right)
            {
                Random r = new Random();
                speed_top += 2;
                speed_left = r.Next(0, 10);
                speed_top = -speed_top;
                point += 1;
                task.Text = point.ToString();
                rakete.Width = r.Next(50, 300);
                ball.BackColor = Color.FromArgb(r.Next(0, 154), r.Next(0, 154), r.Next(0, 154));
                playground.BackColor = Color.FromArgb(r.Next(150, 255), r.Next(150, 255), r.Next(150, 255));
            }
            if (ball.Left <= playground.Left)
            {
                speed_left = -speed_left;
            }
            if (ball.Right >= playground.Right)
            {
                speed_left = -speed_left;
            }
            if (ball.Top <= playground.Top)
            {
                speed_top = -speed_top;
            }
            if (ball.Bottom >= playground.Bottom)
            {
                timer1.Enabled = false;
                game_over.Visible = true;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)

            {
                this.Close();
            }
            if (e.KeyCode == Keys.Space)
            {
                ball.Top = 50;
                ball.Left = 50;
                speed_left = 4;
                speed_top = 4;
                point = 0;
                task.Text = "0";
                timer1.Enabled = true;
                game_over.Visible = false;
                rakete.Width = 200;
                rakete.Height = 20;

            }
        }
    }
}
